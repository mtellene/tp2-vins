package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class MainActivity extends AppCompatActivity {

    public WineDbHelper dbHelper;
    public SimpleCursorAdapter adapter;
    public ListView listview;


    //get all informations
    final String[] colFrom = new String[] {"ROWID AS _id",WineDbHelper._ID,WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_WINE_REGION,
            WineDbHelper.COLUMN_LOC,WineDbHelper.COLUMN_CLIMATE,WineDbHelper.COLUMN_PLANTED_AREA};

    final int[] intTo = new int[] {R.id.name,R.id.region};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //delete db -> no repetition of the list
        //this.deleteDatabase("wine.db");

        dbHelper = new WineDbHelper(this);
        //drop the table if it exists
        dbHelper.dropTable(dbHelper.getReadableDatabase());
        //add tous les vins dans la bdd
        dbHelper.populate();
        //cursor which will fetch wines
        final Cursor cursor = dbHelper.fetchAllWines();

        adapter = new SimpleCursorAdapter(
                this,
                R.layout.row,
                cursor,
                new String[] {WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_WINE_REGION},    //wanted informations
                intTo,
                0
        );
        //RowAdapter adapter = new RowAdapter(this,db);
        listview = (ListView) findViewById(R.id.listOfWines);
        listview.setAdapter(adapter);

        //change activity with a click on an item
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                Cursor cursor = (Cursor)parent.getItemAtPosition(position);
                //get the clicked wine
                Wine wineWanted = WineDbHelper.cursorToWine(cursor);
                //from MainActivity to WineActivity
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                //information(s) sent
                intent.putExtra("wine",(Parcelable) wineWanted);
                startActivityForResult(intent,1);
            }
        });

        registerForContextMenu(listview);
        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener(){
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MainActivity.super.onCreateContextMenu(menu,v,menuInfo);
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menu_main,menu);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine wine = new Wine();
                //add the wine to the list
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", (Parcelable) wine);
                intent.putExtra("add",true);
                startActivityForResult(intent,1);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent dataIntent){
        super.onActivityResult(requestCode,resultCode,dataIntent);
        if(dataIntent!= null) {
            Wine wine = dataIntent.getParcelableExtra("wine");
            boolean add = (dataIntent.getExtras()).getBoolean("add");
            assert wine != null;
            if (add) {
                boolean insert = dbHelper.addWine(wine);
                if (insert) {
                    adapter.changeCursor(dbHelper.getReadableDatabase().query(
                            WineDbHelper.TABLE_NAME,
                            colFrom,
                            null,
                            null,
                            null,
                            null,
                            null));

                    //refresh the listView
                    adapter.notifyDataSetChanged();
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                    alertDialogBuilder.setTitle("Ajout impossible");
                    alertDialogBuilder.setMessage("Un vin portant le même nom existe déjà dans la base de données.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();

                }
            } else { dbHelper.updateWine(wine); }
        }
            // change the cursor
            adapter.changeCursor(dbHelper.getReadableDatabase().query(
                    WineDbHelper.TABLE_NAME,
                    colFrom,
                    null,
                    null,
                    null,
                    null,
                    null));

            //refresh the listView
            adapter.notifyDataSetChanged();
    }
    //to resume the activity
    @Override
    protected void onResume(){
        super.onResume();
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(item.getItemId() == R.id.action_delete) {
            Cursor cursorWine = (Cursor) listview.getItemAtPosition(info.position);
            dbHelper.deleteWine(cursorWine);
            // change the cursor
            adapter.changeCursor(dbHelper.getReadableDatabase().query(
                    WineDbHelper.TABLE_NAME,
                    colFrom,
                    null,
                    null,
                    null,
                    null,
                    null
            ));
            //refresh the listView
            adapter.notifyDataSetChanged();
            return true;
        }
        return super.onContextItemSelected(item);
    }
}

