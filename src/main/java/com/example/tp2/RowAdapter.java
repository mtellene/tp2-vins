package com.example.tp2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

class RowAdapter extends ArrayAdapter<Wine> {

    public RowAdapter(Context context, Wine[] list) {
        super(context, 0 ,list);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Wine wine = getItem(position);
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView region = (TextView) convertView.findViewById(R.id.region);

        name.setText(wine.getTitle());
        region.setText(wine.getRegion());

        return convertView;
    }
}
