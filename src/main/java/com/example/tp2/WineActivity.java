package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class WineActivity extends AppCompatActivity {

    public long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        final Wine wine = (Wine) getIntent().getExtras().get("wine");
        final boolean add = (boolean) getIntent().getExtras().getBoolean("add");

        id = wine.getId();
        final String idString = Long.toString(id);
        final EditText wineAppelation = (EditText)findViewById(R.id.wineName);
        wineAppelation.setText(wine.getTitle());

        final EditText wineRegion = (EditText)findViewById(R.id.editWineRegion);
        wineRegion.setText(wine.getRegion());

        final EditText wineLoc = (EditText)findViewById(R.id.editLoc);
        wineLoc.setText(wine.getLocalization());

        final EditText wineClimate = (EditText)findViewById(R.id.editClimate);
        wineClimate.setText(wine.getClimate());

        final EditText winePlantedArea = (EditText)findViewById(R.id.editPlantedArea);
        winePlantedArea.setText(wine.getPlantedArea());

        //button save
        final Button save = (Button)findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wineId = idString;
                long idWine = Long.parseLong(wineId);
                String editAppelation = wineAppelation.getText().toString();
                String editRegion = wineRegion.getText().toString();
                String editLoc = wineLoc.getText().toString();
                String editClimate = wineClimate.getText().toString();
                String editPlantedArea = winePlantedArea.getText().toString();

                Wine wineToChange = new Wine(idWine, editAppelation, editRegion, editLoc, editClimate, editPlantedArea);

                if(editAppelation.isEmpty()){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                    alertDialogBuilder.setTitle("Sauvegarde impossible");
                    alertDialogBuilder.setMessage("Le nom du vin doit être non vide.");
                    alertDialogBuilder.create();
                    alertDialogBuilder.show();
                }else {

                    Snackbar.make(v, "Wine updated", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                    Intent intent2 = new Intent(WineActivity.this, MainActivity.class);
                    intent2.putExtra("wine", (Parcelable) wineToChange);
                    if(add){ intent2.putExtra("add", true); }
                    else{ intent2.putExtra("add", false); }
                    setResult(1, intent2);
                    finish();
                }
            }
        });
    }
}

