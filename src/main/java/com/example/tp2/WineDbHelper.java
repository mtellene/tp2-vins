package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcel;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    static final String SQL_Create = "CREATE TABLE " + TABLE_NAME + " (" + _ID +" AUTO_INCREMENT," + COLUMN_NAME + " TEXT,"
            + COLUMN_WINE_REGION + " TEXT," + COLUMN_LOC + " TEXT," + COLUMN_CLIMATE + " TEXT," + COLUMN_PLANTED_AREA
            + " TEXT, UNIQUE (" + COLUMN_NAME + ", " + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK)";

    static final String SQL_Delete = "DROP TABLE IF EXISTS " + TABLE_NAME;


    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_Create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //discard data and start over
        db.execSQL(SQL_Delete);
        onCreate(db);
    }

    public void dropTable(SQLiteDatabase db){
        db.execSQL(SQL_Delete);
        onCreate(db);
    }

    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        //gets data repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();

        //create a new map of values
        ContentValues values = new ContentValues();
        values.put(_ID, wine.getId());
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_CLIMATE, wine.getClimate());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        long rowID = 0;
        try {
            rowID = db.insert(TABLE_NAME, null, values);
        }catch(SQLiteConstraintException e){
            System.out.println(e.getMessage());
        }
        //insert rows
        db.close(); // Closing database connection

        //-1 if error
        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        int res = 0;

        //updating row
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, wine.getTitle());
        values.put(COLUMN_WINE_REGION, wine.getRegion());
        values.put(COLUMN_LOC, wine.getLocalization());
        values.put(COLUMN_CLIMATE, wine.getClimate());
        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());


        /*String selection = WineDbHelper.COLUMN_NAME + " LIKE ?";
        String[] selectionArgs = {wine.getTitle()};*/

        String selection = WineDbHelper._ID + " LIKE ?";
        String[] selectionArgs = { Long.toString(wine.getId())};

        // call db.update()
        res = db.update(
                TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */

    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        //which columns I will use after the query
        String[] projection = {
                WineDbHelper._ID,
                WineDbHelper.COLUMN_NAME,
                WineDbHelper.COLUMN_WINE_REGION,
                WineDbHelper.COLUMN_LOC,
                WineDbHelper.COLUMN_CLIMATE,
                WineDbHelper.COLUMN_PLANTED_AREA
        };

        // call db.query()
        Cursor cursor = db.query(
                WineDbHelper.TABLE_NAME,
                projection,
                null,       //col for where clause
                null,    //value for where clause
                null,    //group by
                null,
                null);

        List wines = new ArrayList<>();
        if (cursor != null) {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                String itemName = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                String itemRegion = cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION));
                wines.add(itemName);
                wines.add(itemRegion);
            }
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();

        String selection = WineDbHelper._ID + " LIKE ?";
        String[] selectionArgs = { Long.toString(cursorToWine(cursor).getId())};
        // call db.delete();
        db.delete(
                TABLE_NAME,
                selection,
                selectionArgs
        );
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine(1,"Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine(2,"Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine(3,"Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine(4,"Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine(5,"Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine(6,"Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine(7,"Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine(8,"Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine(9,"Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine(10,"Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine(11,"Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
        long id = cursor.getLong(cursor.getColumnIndex(_ID));
        String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        String region = cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION));
        String loc = cursor.getString(cursor.getColumnIndex(COLUMN_LOC));
        String climate = cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE));
        String plantedArea = cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA));

        wine = new Wine(id,name,region,loc,climate,plantedArea);
        return wine;
    }
}